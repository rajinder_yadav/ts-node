{
  "name": "@dm-tools/demo",
  "version": "1.0.0",
  "description": "DM-Tools generated TypeScript Koa Node.js demo Server with static Website",
  "main": "build/server.js",
  "scripts": {
    "build": "tsc",
    "browser": "/usr/bin/google-chrome-stable --new-window || /usr/bin/brave-browser --new-window",
    "clean": "shx rm -rf build",
    "copy:key": "cp -r key build/",
    "debug:node": "cross-env PORT=3100 NODE_ENV=development node --inspect-brk build/server.js",
    "debug": "gazeall -V --npms 'clean dev:build debug:node' -w 'src/**/*'",
    "dev": "gazeall -V --npms 'clean format lint dev:build copy:key dev:watch' -w 'src/**/*'",
    "dev:build": "tsc -p ./tsconfig.dev.json",
    "dev:watch": "cross-env PORT=3100 NODE_ENV=development node build/server.js",
    "doc": "typedoc --plugin typedoc-plugin-markdown --out docs/typedoc",
    "format": "biome format --write ./src",
    "lint": "biome lint --write ./src",
    "log": "tail -n 20 logs/server.log -f",
    "prebuild": "run-s format clean lint",
    "precommit": "run-s format lint",
    "predebug": "run-s clean dev:build",
    "predoc": "shx rm -rf docs/typedoc && shx mkdir -p docs/typedoc",
    "prepush": "npm run test",
    "prestart": "npm run release",
    "pretest": "run-s clean dev:build",
    "release": "run-s clean format lint build copy:key",
    "start": "cross-env PORT=5000 NODE_ENV=production node build/server.js",
    "test": "jest './build/'",
    "test:coverage": "npm run test -- --coverage",
    "test:watch": "gazeall -V --delay 3000 --npms 'clean dev:build test' -w 'src/**/*'",
    "sig:hup": "kill -1 $(lsof -i :3100  |grep node|cut -d' ' -f5)",
    "sig:term": "kill -15 $(lsof -i :3100|grep node|cut -d' ' -f5)",
    "sig:kill": "kill -9 $(lsof -i :3100 |grep node|cut -d' ' -f5)"
  },
  "keywords": [
    "javascript",
    "js",
    "ts",
    "typescript",
    "nodejs",
    "web",
    "css",
    "sass"
  ],
  "author": "[Fullname] <your-email>",
  "license": "GPL-3.0",
  "repository": {
    "type": "git",
    "url": "https://github.com/<github-user-id>/<project-name>"
  },
  "dependencies": {
  },
  "devDependencies": {
  }
}
