/**
 * koa-better-router: https://www.npmjs.com/package/koa-better-router
 */
import Router from '@koa/router';

export const routerAdmin = new Router({prefix: '/admin'});

/**
 * Sample Router middleware: Send a respond with 'Pong' as the message.
 * GET /admin/ping
 */
routerAdmin.get('/ping', (ctx, next) => {
  ctx.response.body = {status: 200, message: 'Pong'};
  return next();
});

/**
 * Sample Router middleware: Send a response with message from the request.
 * POST /admin/ping
 */
routerAdmin.post('/ping', (ctx, next) => {
  ctx.response.body = {status: 200, message: ctx.request.body};
  return next();
});
