'use strict';

import {inspect} from 'node:util';
import {hrtime} from 'node:process';

/**
 * Logger middleware
 * async: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
 * await: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await
 */
export async function requestLogger(ctx, next) {
  const startTime = hrtime.bigint();
  await next();
  const endTime = hrtime.bigint();
  ctx.log.info(
    `REQUEST: ${ctx.method} ${ctx.url} - execution: ${endTime - startTime} ns`,
  );
  ctx.log.info(
    `HEADER: ${inspect(ctx.header)}} - execution: ${endTime - startTime} ns`,
  );
  ctx.log.info(
    `BODY: ${inspect(ctx.request.body)} - execution: ${endTime - startTime} ns`,
  );
}
