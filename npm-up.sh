#!/usr/bin/env bash
# Update Script for NPM Modules.
# Run this to install the latest modules and update package.json
# package.json.t is the template file, make changes there!

cp package.json.t package.json
rm -f pnpm-lock.yaml package-lock.json yarn.lock
rm -rf node_modules

pnpm i -S bcrypt bunyan bunyan-format jsonwebtoken koa @koa/router koa-body @koa/cors koa-helmet axios
pnpm i -D cross-env cross-var gazeall npm-run-all shx jest biome
pnpm i -D typedoc typedoc-plugin-markdown typescript tape-run
pnpm i -D eslint esdoc @typescript-eslint/parser @typescript-eslint/eslint-plugin eslint-config-prettier

pnpm i -D @types/bunyan @types/bunyan-format @types/shelljs @types/jest
pnpm i -D @types/node @types/koa @types/koa__cors @types/koa__router
pnpm i -D @types/axios @types/jsonwebtoken

# ppm i -D mocha chai sinon
# ppm i -D @types/chai @types/sinon @types/mocha

if [ "$1" = "-" ]; then
  rm -rf node_modules
  # rm -f package-lock.json yarn.lock
fi
