@PORT=3100
@HOST=localhost

###
get http://{{HOST}}:{{PORT}}/hello

###
get http://{{HOST}}:{{PORT}}/admin/ping

###
post http://{{HOST}}:{{PORT}}/admin/ping
Content-Type: application/json

{
  "name": "Rajinder"
}

### Path not defined
get http://{{HOST}}:{{PORT}}/xyz

### API not defined
delete http://{{HOST}}:{{PORT}}


###
get http://{{HOST}}:{{PORT}}/error/request
###
get http://{{HOST}}:{{PORT}}/error/server

############################
### Test JWT using a secert
############################

### Login 1 - Valid User
POST http://{{HOST}}:{{PORT}}/api/v1/login/secret
Content-Type: application/json

{
  "name": "Rajinder", "password":"ldkfj34kjrpoiro34lj5l345"
}

### Protect route valid
GET http://{{HOST}}:{{PORT}}/api/v1/protected/secret
Content-Type: application/json

### Login 2 - Invalid User
POST http://{{HOST}}:{{PORT}}/api/v1/login/secret
Content-Type: application/json

{
  "name": "Shivaji"
}

#############################
### Test JWT using a SSL keys
#############################

### Login 1 - Valid User
POST http://{{HOST}}:{{PORT}}/api/v1/login/key
Content-Type: application/json

{
  "name": "Rajinder", "password":"ldkfj34kjrpoiro34lj5l345"
}

### Protect route valid
GET http://{{HOST}}:{{PORT}}/api/v1/protected/key
Content-Type: application/json

### Protect route valid
GET http://{{HOST}}:{{PORT}}/api/v1/protected/bearer
Content-Type: application/json
Authorization: Bearer ng eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiUmFqaW5kZXIiLCJleHAiOjE3MzY3MzMwNjUsImlhdCI6MTczNjczMzA0NX0.TrCrSMcafPNBJ3071csnRnhVynXbxf1hhe-vYVAm3n4EAwii1AxRBuWM7e5krw5ArW6nyooFK1HrK8ai0nGWPMD-W38EtyZ89WrEKYi4YtsfjpgNOT2_PpiWcyJeHxOVbvb_fZFe165OciW9XehoMjFX8FhSKTaIAjzCcYqoY_DPbz2gBgNG6TVgWDp1TifENfZvENAZ9Ofn2kwhdjJateEOTWfayh5bEPp1d5DTSjPT5nW9RqOfg-tvKOFdl5hLPY0DWngEVD7_y1cQQM3B4dxIoaj8TkhyFCmbrL1K6GoeeBy0T9t0cww-o6RgP01aePq-n2rMuk1YqvjeKNa9_Osbs_OfirnNIWv9FlSLYkM2m3e1vsGwf9AZNBp6RG7WA96wT2EmeYH1ElUXm15qjnKfvo4yyYoiEjzWG2Ocpl8MMeLnLm6uxucf8AsjCwDFk_Eka7oYc_vUc2-VkWX25fyuzKhT-216iP7h_8xDGMkJbL4WNKb75FOfW54jHgOLn4yVWSqpX1jQg1cyTI3RdZgxdSYHMx6dGszZAGIaw2RktyVxeO3VW_Wydi3BV4TEqci3vAKgGov72Q0FwVtT78STJUZOZYjf98nHG2_dSb_MZxxas6DX7F-crZA_9TXPZ2CXUCZgcJkT4cwM1si6jpvX6hicGYyz7tWXEwCEbhw

### Login 2 - Invalid User
POST http://{{HOST}}:{{PORT}}/api/v1/login/key
Content-Type: application/json

{
  "name": "Shivaji"
}
